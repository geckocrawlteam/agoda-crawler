let EventEmitter=require("events");

class StatStream extends EventEmitter {
	constructor(stream) {
		super();

		if (!stream)
			throw new Error("StatStream: no stream");

		this.stream=stream;
		this.numWritten=0;
		this.haveEof=false;
		this.haveError=false;
		this.waterMark=0;
	}

	async read() {
		try {
			let o=await this.stream.read();
			this.waterMark--;
			this.emit("waterMarkChange")
			return o;
		}

		catch (e) {
			this.waterMark--;
			this.emit("waterMarkChange")
			throw(e);
		}
	}

	write(o) {
		if (o instanceof Promise)
			throw new Error("StatStream::write(Promise) not implemented.");

		if (o && !(o instanceof Error))
			this.numWritten++;

		if (!o)
			this.haveEof=true;

		if (o instanceof Error)
			this.haveError=true;

		this.stream.write(o);
		this.waterMark++;
		this.emit("waterMarkChange")
	}

	drain(level) {
		if (!level)
			level=0;

		if (this.waterMark<=level)
			return Promise.resolve();

		return new Promise((resolve,reject)=>{
			let onWaterMarkChange=()=>{
				if (this.waterMark<=level) {
					this.removeListener("waterMarkChange",onWaterMarkChange);
					resolve();
				}
			}
			this.on("waterMarkChange",onWaterMarkChange);
		});
	}

	getWaterMark() {
		return this.waterMark;
	}

	getNumWritten() {
		return this.numWritten;
	}

	isWriteOpen() {
		if (this.haveEof || this.haveError)
			return false;

		return true;
	}
}

module.exports=StatStream;