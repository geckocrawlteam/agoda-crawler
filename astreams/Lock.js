class Lock {
	constructor() {
		this.numParallell=4;
		this.numHeld=0;
		this.queue=[];
	}

	setNumParallell(value) {
		if (!value)
			throw new Error("Number of parallell locks can't be 0");

		this.numParallell=value;
	}

	aquire() {
		if (this.numHeld<this.numParallell) {
			this.numHeld++;
			return Promise.resolve();
		}

		return new Promise((resolve,reject)=>{
			this.queue.push(resolve);
		});
	}

	release() {
		this.numHeld--;
		if (this.numHeld<0)
			throw new Error("Number of held locks<0");

		if (this.numHeld<this.numParallell
				&& this.queue.length>0) {
			this.numHeld++;
			let resolve=this.queue.shift();
			resolve();
		}
	}
}

module.exports=Lock;