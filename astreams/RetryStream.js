const Stream=require("./Stream.js");
const StreamSink=require("./StreamSink.js");

class RetryStream {
	constructor(factory) {
		this.out=new Stream();
		this.started=false;
		this.factory=null;
		this.timeout=30000;
		this.logger=null;
		this.retries=4
		this.retry=0;
		this.failError=null;
		this.fatalTypes=[];

		this.onStreamSinkItem=this.onStreamSinkItem.bind(this);
		this.onTimeout=this.onTimeout.bind(this);
		this.resetTimeout=this.resetTimeout.bind(this);

		this.setFactory(factory);
	}

	addFatal(fatalType) {
		if (!(fatalType.prototype instanceof Error))
			throw new Error("The fatal error should inherit from error.");

		this.fatalTypes.push(fatalType);
	}

	setRetries(retries) {
		this.retries=retries;
	}

	setLogger(logger) {
		this.logger=logger;
	}

	setFactory(factory) {
		this.factory=factory;
	}

	setTimeout(timeout) {
		this.timeout=timeout;
		this.resetTimeout();
	}

	log(message) {
		if (this.logger)
			this.logger(message);
	}

	restart() {
		if (!this.factory) {
			this.out.write(new Error("No factory set"));
			return;
		}

		if (this.streamSink)
			this.streamSink.removeListener("item",this.onStreamSinkItem);

		if (this.timeoutId) {
			clearTimeout(this.timeoutId);
			this.timeoutId=null;
		}

		this.started=true;
		this.stream=null;
		this.streamSink=null;

		Promise.resolve(this.factory())
			.then((stream)=>{
				if (!stream) {
					this.out.write(new Error("No stream produced."));
					return;
				}
				this.log("RetryStream: Factory created, starting...");
				this.stream=stream;
				this.streamSink=new StreamSink(this.stream);
				this.streamSink.on("item",this.onStreamSinkItem);
				this.timeoutId=setTimeout(this.onTimeout,this.timeout);
			})
			.catch((err)=>{
				this.log("RetryStream: factory failed");
				this.out.write(err);
			});
	}

	onStreamSinkItem(item) {
		//this.log("got stream sink item: "+item);
		if (item instanceof Error) {
			this.log("!! RetryStream: Got Error: "+new String(item));

			let fatal=false;
			for (let errorType of this.fatalTypes)
				if (item instanceof errorType)
					fatal=true;

			this.retry++;
			if (this.retry>=this.retries || fatal) {
				if (fatal)
					this.log("Got a fatal error, giving up...");

				else
					this.log("Tried "+this.retry+" times, giving up...");

				if (this.timeoutId) {
					clearTimeout(this.timeoutId);
					this.timeoutId=null;
				}

				this.failError=item;
				this.out.write(this.failError);
				return;
			}

			this.log("Retry #"+this.retry+", restarting...");
			this.restart();
		}

		else {
			this.resetTimeout();
			this.retry=0;
			this.out.write(item);
		}
	}

	resetTimeout() {
		if (!this.timeoutId)
			return;

		clearTimeout(this.timeoutId);
		this.timeoutId=setTimeout(this.onTimeout,this.timeout);
	}

	onTimeout() {
		this.log("!! RestryStream: Timeout ");

		this.retry++;
		if (this.retry>=this.retries) {
			this.log("Tried "+this.retry+" times, giving up...");
			this.failError=new Error("Stream timed out");
			this.out.write(this.failError);
			return;
		}

		this.log("Timeout, retry #"+this.retry+", restarting...");
		this.restart();
	}

	async read() {
		if (this.failError)
			throw this.failError;

		if (!this.started)
			this.restart();

		return this.out.read();
	}
}

module.exports=RetryStream;