class AsyncStream {
	constructor() {
		this.queue=[];
		this.waiting=[];
	}

	write(o) {
		if (o instanceof Promise)
			o.catch(()=>{});

		if (this.waiting.length) {
			let resolver=this.waiting.shift();

			if (o instanceof Error)
				resolver.reject(o);

			else
				resolver.resolve(o);
		}

		else
			this.queue.push(o);
	}

	async read() {
		if (this.queue.length) {
			let o=await Promise.resolve(this.queue.shift());
			//let o=this.queue.shift();

			if (o instanceof Error)
				throw o;

			return o;
		}

		return new Promise((resolve, reject)=>{
			this.waiting.push({
				resolve,
				reject
			});
		});
	}
}

module.exports=AsyncStream;