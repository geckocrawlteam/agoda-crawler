const EventEmitter=require("events");

class StreamSink extends EventEmitter {
	constructor(stream) {
		super();

		if (!stream)
			throw new Error("Stream required for event sink");

		this.stream=stream;
		process.nextTick(this.readNext.bind(this));
	}

	getStream() {
		return this.stream;
	}

	readNext() {
		this.stream.read()
			.then((o)=>{
				this.emit("item",o);
				if (o)
					process.nextTick(this.readNext.bind(this));
			})
			.catch((o)=>{
				this.emit("item",o);
				process.nextTick(this.readNext.bind(this));
			});
	}
}

module.exports=StreamSink;