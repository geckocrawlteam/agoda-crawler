const Stream=require("./Stream.js");
const StreamSink=require("./StreamSink.js");
const Lock=require("./Lock.js");

class FilterStream {
	constructor(filter) {
		this.in=new Stream();
		this.inSink=new StreamSink(this.in);
		this.inSink.on("item",this.onInItem.bind(this));
		this.out=new Stream();
		this.filter=null;
		this.lock=new Lock();

		if (filter)
			this.setFilter(filter);
	}

	setNumParallell(value) {
		this.lock.setNumParallell(value);
	}

	setFilter(filter) {
		this.filter=filter;
	}

	onInItem(item) {
		if (!item
				|| item instanceof Error
				|| !this.filter)
			this.out.write(item);

		else 
			this.out.write(new Promise((resolve, reject)=>{
				this.lock.aquire().then(()=>{
					Promise.resolve(this.filter(item))
						.then((item)=>{
							resolve(item);
							this.lock.release();
						})
						.catch((err)=>{
							reject(err);
							this.lock.release();
						});
				});
			}));
	}

	write(o) {
		this.in.write(o);
	}

	async read() {
		return this.out.read();
	}
}

module.exports=FilterStream;