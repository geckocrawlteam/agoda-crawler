const AgodaCrawlStream=require("./AgodaCrawlStream.js");
const crawlutil=require("./crawlutil.js");
const ImageAborter=require("./ImageAborter.js");
const astreams=require("./astreams/astreams.js");
const XhrCap=require("./XhrCap.js");
const NetSpy=require("./NetSpy.js");

module.exports=async function(crawl) {
	crawl.input.results=crawlutil.clampNum(crawl.input.results,1,10000);
	crawl.input.rooms=crawlutil.clampNum(crawl.input.rooms,1,10);
	crawl.input.daysfromnow=crawlutil.clampNum(crawl.input.daysfromnow,1,90);

	let imageAborter=new ImageAborter();
	let numResults=0;

	let agodaStream;
	let crawlStream=new astreams.RetryStream();
	crawlStream.setLogger(crawl.log);
	crawlStream.addFatal(crawlutil.NoRetryError);

	let propertySpy;

	crawlStream.setFactory(async ()=>{
		if (agodaStream) {
			crawl.log("Stopping old stream...");
			await agodaStream.stop();
			agodaStream.removeAllListeners();
			crawl.log("Renewing browser page...");
			await crawl.renewPage();
			crawl.log("Done cleaning up old stream...");
		}

		await imageAborter.attach(crawl.page);

		/*propertySpy=new NetSpy();
		propertySpy.addFilter(NetSpy.XhrFilter);
		propertySpy.addFilter(NetSpy.UrlSubstringFilter("property"));
		propertySpy.setConsoleLogger(crawl.log);
		propertySpy.setCaptureLogDir("requestlog");
		propertySpy.attach(crawl.page);*/

		agodaStream=new AgodaCrawlStream(crawl,numResults);
		agodaStream.on("activity",crawlStream.resetTimeout);
		agodaStream.start();

		return agodaStream;
	});

	let o=await crawlStream.read();

	while (o) {
		crawl.result(o);
		numResults++;
		crawl.progress(100*numResults/crawl.input.results);
		o=await crawlStream.read();
	}

	return "done";
};