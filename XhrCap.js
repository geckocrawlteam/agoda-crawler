const EventEmitter=require("events");

class XhrCap extends EventEmitter {

	constructor(page) {
		super();

		this.onRequest=this.onRequest.bind(this);
		this.onRequestFinishedOrFailed=this.onRequestFinishedOrFailed.bind(this);

		if (page)
			this.attach(page);

		this.filter=null;
		this.consoleLogger=null;
		this.requests=[];
		this.numFinishedRequests=0;
	}

	setFilter(filter) {
		this.filter=filter;
	}

	setConsoleLogger(logger) {
		this.consoleLogger=logger;
	}

	log(message) {
		if (this.consoleLogger)
			this.consoleLogger(message);
	}

	onRequest(request) {
		if (this.filter 
				&& request.url().indexOf(this.filter)<0)
			return;

		this.requests.push(request);
		this.log("XhrCap("+this.filter+"): Send...");
	}

	onRequestFinishedOrFailed(request) {
		if (this.requests.indexOf(request)<0)
			return;

		if (!request.response()) {
			this.log("XhrCap("+this.filter+"): No response.");
			this.emit("requestfailed",new Error("Empty response"));
			this.removeRequest(request);
			return;
		}

		request.response().text()
			.then((body)=>{
				this.log("XhrCap("+this.filter+"): Complete.");
				this.emit("response",request.response(),body);
				this.removeRequest(request);
			})
			.catch((err)=>{
				this.requests.splice(this.requests.indexOf(request),1);
				this.log("XhrCap("+this.filter+"): Fail.");
				//console.log(err);
				this.emit("requestfailed",err);
				this.removeRequest(request);
			});
	}

	removeRequest(request) {
		this.requests.splice(this.requests.indexOf(request),1);
		this.numFinishedRequests++;
		this.emit("statechange");
	}

	attach(page) {
		this.page=page;

		this.page.on("request",this.onRequest);
		this.page.on("requestfinished",this.onRequestFinishedOrFailed);
		this.page.on("requestfailed",this.onRequestFinishedOrFailed);
	}

	detach() {
		if (this.page) {
			this.page.removeListener("request",this.onRequest);
			this.page.removeListener("requestfinished",this.onRequestFinishedOrFailed);
			this.page.removeListener("requestfailed",this.onRequestFinishedOrFailed);
			this.page=null;
		}
	}

	waitForResponse() {
		return new Promise((resolve, reject)=>{
			let startNum=this.numFinishedRequests;

			let f=()=>{
				if (this.numFinishedRequests>startNum) {
					this.removeListener("statechange",f);
					resolve();
				}
			}

			this.on("statechange",f);
		});
	}

	isIdle() {
		if (!this.requests.length)
			return true;

		else
			return false;
	}

	waitForIdle() {
		if (this.isIdle())
			return Promise.resolve();

		return new Promise((resolve, reject)=>{
			let f=()=>{
				if (this.isIdle()) {
					this.removeListener("statechange",f);
					resolve();
				}
			}

			this.on("statechange",f);
		});
	}
}

module.exports=XhrCap;