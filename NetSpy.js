const fs=require("fs-extra");
const { URL } = require('url');

function pad(num, size=4) {
	num=new String(num);
	while (num.length<size)
		num="0"+num;

	return num.toString();
}

class NetSpy {

	constructor() {
		this.onRequest=this.onRequest.bind(this);
		this.onRequestFinishedOrFailed=this.onRequestFinishedOrFailed.bind(this);
		this.filters=[];
		this.capturing=[];
		this.consoleLogger=null;
	}

	setConsoleLogger(logger) {
		this.consoleLogger=logger;
	}

	setCaptureLogDir(dir) {
		this.captureLogDir=dir;
	}

	log(message) {
		if (this.consoleLogger)
			this.consoleLogger(message);
	}

	getRequestSlug(request) {
		let u=new URL(request.url());
		let slug=u.pathname.split("/").filter(x=>x).pop();
		if (!slug)
			slug="";

		return slug;
	}

	getRequestDir(request) {
		let reqIndex=this.capturing.indexOf(request);
		if (reqIndex<0)
			throw new Error("Not capturing");

		let label=this.getRequestSlug(request);
		return this.captureLogDir+"/"+pad(reqIndex,3)+"-"+label;
	}

	onRequest(request) {
		let capture=true;

		for (let filter of this.filters)
			capture=(capture && filter(request))

		if (capture) {
			let reqIndex=this.capturing.length;
			this.capturing.push(request);

			this.log("Req #"+reqIndex+": "+this.getRequestSlug(request));

			if (this.captureLogDir) {
				let dir=this.getRequestDir(request);
				fs.ensureDirSync(dir);

				let req={
					"URL": request.url(),
					"METHOD": request.method()
				};

				req={...req,...request.headers()};
				fs.writeFileSync(dir+"/request.json",JSON.stringify(req,null,"\t"));

				if (request.postData())
					fs.writeFileSync(dir+"/request-body.txt",request.postData());
			}
		}
	}

	onRequestFinishedOrFailed(request) {
		let index=this.capturing.indexOf(request);
		if (index<0)
			return;

		if (this.captureLogDir) {
			let response=request.response();
			if (response) {
				let dir=this.getRequestDir(request);

				let resp={
					"STATUS": response.status()
				};

				resp={...resp,...response.headers()};
				fs.writeFileSync(dir+"/response.json",JSON.stringify(resp,null,"\t"));
				response.text()
					.then((text)=>{
						fs.writeFileSync(dir+"/response-body.txt",text);
					})
					.catch((err)=>{
						if (err.message)
							err=err.message;

						this.log("RequestFailed #"+index+": "+err);
					});
			}
		}

		if (this.responseResolver && !this.getNumPending()) {
			this.responseResolver.resolve();
			this.responseResolver=null;
			this.responsePromise=null;
		}
	}

	attach(page) {
		this.page=page;
		this.page.on("request",this.onRequest);
		this.page.on("requestfinished",this.onRequestFinishedOrFailed);
		this.page.on("requestfailed",this.onRequestFinishedOrFailed);
	}

	detach() {
		if (this.page) {
			this.page.removeListener("request",this.onRequest);
			this.page.removeListener("requestfinished",this.onRequestFinishedOrFailed);
			this.page.removeListener("requestfailed",this.onRequestFinishedOrFailed);
			this.page=null;
		}
	}

	async waitForResponses() {
		if (this.responsePromise)
			return this.responsePromise;

		if (!this.getNumPending())
			return Promise.resolve();

		this.responsePromise=new Promise((resolve, reject)=>{
			this.responseResolver={
				resolve: resolve,
				reject: reject
			}
		});

		return this.responsePromise;
	}

	addFilter(filter) {
		this.filters.push(filter);
	}

	getNumRequests() {
		return this.capturing.length;
	}

	getNumPending() {
		let pending=0;

		for (let request of this.capturing)
			if (!request.response())
				pending++;

		return pending;
	}

	getResponses() {
		let ret=[];

		for (let request of this.capturing)
			ret.push(request.response());

		return ret;
	}

	getFirstRequest() {
		if (!this.capturing.length)
			throw new Error("No requests!");

		return this.capturing[0];
	}

	static XhrFilter() {
		return (request)=>{
			return request.resourceType()=="xhr";
		};
	}

	static UrlSubstringFilter(substr) {
		return (request)=>{
			if (request.url().indexOf(substr)<0)
				return false;

			return true;
		};
	}
}

module.exports=NetSpy;