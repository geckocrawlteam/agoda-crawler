module.exports=async(crawl)=>{
	let stream=new RetryStream();
	let crawler;

	async function createCrawlStream() {
		if (crawler) {
			await crawler.stop();
		}

		crawler=new AgodaCrawlStream(crawl,stream.getNumProduced());
		crawler.start();

		return crawler;
	}

	stream.setTimeout(30000);
	stream.setFactory(createCrawlStream);

	let o=await stream.read();
	while (o) {
		crawl.result(o);
		o=await stream.read();
	}
}
