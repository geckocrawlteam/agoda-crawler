const crawlutil=require("../crawlutil.js");

/*p=new Promise((resolve, reject)=>{
	console.log("creating promise");
});

console.log("here...");*/

async function test() {
	throw new Error("test");
}

//a=test();

a=new Promise((resolve,reject)=>{
	resolve(new Error("test"));
});

Promise.resolve(a)
	.then(()=>{
		console.log("then");
	})
	.catch(()=>{
		console.log("caught");
	})
	.finally(()=>{
		console.log("finally");
	});
/*	.then()
	.catch((err)=>{
		console.log("caught...")
	});*/