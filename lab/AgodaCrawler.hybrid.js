let fs=require("fs-extra");
let NetSpy=require("./NetSpy.js");
let ImageAborter=require("./ImageAborter.js");
let crawlutil=require("./crawlutil.js");
let XhrCap=require("./XhrCap.js");
let moment=require("moment");

class AgodaCrawl {
	constructor(crawl) {
		this.crawl=crawl;
		this.page=this.crawl.getPage();
		this.result=[];
		this.listResults=[];
		this.foundLastPage=false;

		this.netSpy=new NetSpy();
		this.netSpy.addFilter(NetSpy.XhrFilter());
		this.netSpy.attach(this.page);
		this.netSpy.setConsoleLogger(this.crawl.log);
		//this.netSpy.setCaptureLogDir("./requestlog/"+Date.now());

		this.imageAborter=new ImageAborter();
		this.imageAborter.attach(this.page);

		this.onResultListResponse=this.onResultListResponse.bind(this);
		this.onResultListFailed=this.onResultListFailed.bind(this);

		this.cleanInput();
	}

	cleanInput() {
		this.crawl.input.results=crawlutil.clampNum(this.crawl.input.results,1,2000);
		this.crawl.input.rooms=crawlutil.clampNum(this.crawl.input.rooms,1,10);
		this.crawl.input.daysfromnow=crawlutil.clampNum(this.crawl.input.daysfromnow,1,90);
	}

	async getResultUrlForSearch(search) {
		await this.page.goto("http://www.agoda.com/",{waitUntil: "domcontentloaded"});
		this.crawl.log("Initial page loaded");
		await this.crawl.injectJquery();

		let res=await crawlutil.ajaxRequestInPage(this.page,{
			url: "/Search/Search/GetUnifiedSuggestResult/3/1/1/0/en-us/",
			data: {
				searchText: search
			}
		});

		await this.page.evaluate(()=>{
			window.stop();
		});

		if (res.SuggestionList.length<1)
			throw new crawlutil.NoRetryError("No search results found for "+search);

		let suggestionItem=res.SuggestionList[0];
		if (!suggestionItem.Url)
			throw new crawlutil.NoRetryError("No search results found for "+search);

		this.crawl.progress(10);

		let checkIn=moment().add(this.crawl.input.daysfromnow,"days");

		return "https://www.agoda.com"+suggestionItem.Url+
			"&rooms="+this.crawl.input.rooms+
			"&adults="+this.crawl.input.rooms+
			"&checkIn="+checkIn.format("YYYY-MM-DD")+
			"&checkOut="+checkIn.add(1,"days").format("YYYY-MM-DD");
	}

	createEntry(listResult, propertyResult) {
		let res={
			name: listResult.HotelDisplayName,
			price: listResult.DisplayPrice,
			address: propertyResult.hotelInfo.address.full,
		}

		for (let infoGroup of propertyResult.aboutHotel.usefulInfoGroups) {
			//console.log(infoGroup);
			for (let item of infoGroup.items) {
				//console.log(item);
				if (item.fontIcon=="ficon-number-of-rooms")
					res.numberOfRooms=parseInt(item.description);
			}
		}

		let facilities=[];
		for (let featureGroup of propertyResult.aboutHotel.featureGroups) {
			for (let feature of featureGroup.feature) {
				if (feature.symbol=="ficon-yoga-room")
					facilities.push("Yoga");

				if (feature.symbol=="ficon-kitchen" ||
						feature.symbol=="ficon-kitchenette")
					facilities.push("Kitchen");

				if (feature.symbol=="ficon-spa-sauna")
					facilities.push("Spa");

				if (feature.symbol=="ficon-outdoor-pool")
					facilities.push("Pool");
			}
		}

		facilities.sort();
		let facilitiesClean=[];
		for (let facility of facilities)
			if (facilitiesClean.indexOf(facility)<0)
				facilitiesClean.push(facility);

		res.facilities=facilitiesClean.join(", ");

		return res;
	}

	async processAndAddResult(agodaResult) {
		let url=
			"/api/en-us/pageparams/property?hotel_id="
			+agodaResult.HotelID;

		let propertyResult=await crawlutil.ajaxRequestInPage(this.page,{
			url: url,
		});

		let res=this.createEntry(agodaResult,propertyResult);
		this.result.push(res);

		let fraq=this.result.length/this.crawl.input.results;
		let percent=20+80*fraq;
		if (percent>99)
			percent=99;

		this.crawl.progress(percent);
	}

	isResultDone() {
		if (this.result.length>=this.crawl.input.results)
			return true;

		if (this.listResults.length)
			return false;

		if (this.foundLastPage)
			return true;

		return false;
	}

	async processListResults() {
		while (this.listResults.length && !this.isResultDone()) {
			let listResult=this.listResults.shift();
			await this.processAndAddResult(listResult);
		}
	}

	onResultListResponse(response, body) {
		let res=JSON.parse(body);
		this.crawl.log("Search response page #"+res.PageNumber+": "+res.ResultList.length);

		if (res.ResultList.length==0)
			this.foundLastPage=true;

		this.listResults=[...this.listResults,...res.ResultList];
	}

	onResultListFailed(err) {
		this.crawl.error(err);
	}

	async isOnFinalPage() {
		function isFinalPageRendered() {
			function extractNumberTokens(s) {
				let a=s.split(" ");
				let res=[];

				for (candString of a) {
					let cand=parseInt(candString)
					if (!isNaN(cand) && cand!=0)
						res.push(cand);
				}

				return res;
			}

			let parts=extractNumberTokens($("#paginationContainer").text());
			if (parts.length==2 && parts[0]==parts[1])
				return true;

			return false;
		}

		return await this.page.evaluate(isFinalPageRendered);
	}

	async waitForNavigationElements() {
		function isPageNavigationRendered() {
			function extractNumberTokens(s) {
				let a=s.split(" ");
				let res=[];

				for (candString of a) {
					let cand=parseInt(candString)
					if (!isNaN(cand) && cand!=0)
						res.push(cand);
				}

				return res;
			}

			if ($("#paginationNext").length)
				return true;

			let parts=extractNumberTokens($("#paginationContainer").text());
			if (parts.length==2 && parts[0]==parts[1])
				return true;

			return false;
		}

		await this.page.waitForFunction(isPageNavigationRendered);
	}

	async run() {
		let url=await this.getResultUrlForSearch(this.crawl.input.region);
		this.crawl.log("Opening: "+url);

		this.xhrCap=new XhrCap(this.page);
		this.xhrCap.setConsoleLogger(this.crawl.log);
		this.xhrCap.setFilter("GetSearchResultList");
		this.xhrCap.on("response",this.onResultListResponse);
		this.xhrCap.on("requestfailed",this.onResultListFailed);

		let p=await Promise.all([
			this.page.goto(url,{waitUntil: "domcontentloaded"}),
			this.xhrCap.waitForResponse()
		]);

		await this.crawl.injectJquery();

		let i=0;
		while (!this.isResultDone()) {
			this.crawl.log("** Page loop iteration: "+i);
			await this.processListResults();
			await this.waitForNavigationElements();
			let isOnFinalPage=await this.isOnFinalPage();

			await this.xhrCap.waitForIdle();

			if (isOnFinalPage) {
				this.foundLastPage=true;
				await this.processListResults();
			}

			if (!this.isResultDone() && !isOnFinalPage) {
				let p=await Promise.all([
					this.page.evaluate(()=>{
						$("#paginationNext").click();
					}),
					this.xhrCap.waitForResponse()
				]);

				i++;
			}
		}

		this.crawl.log("Done...");
	}

	getResult() {
		return this.result;
	}
}

module.exports=AgodaCrawl;