const astreams=require("./astreams/astreams.js");
const crawlutil=require("./crawlutil.js");

/*async function main() {
	let stream=new astreams.Stream();
	stream.write(new Promise((resolve,reject)=>{
		reject(new Error("hello"));
	}));

	await crawlutil.sleep(100);
	stream.read().catch((e)=>{});
}*/

async function main() {
	let stream=new astreams.FilterStream(async (x)=>{
//		return "hello: "+x;
		throw new Error("error...");
	});

	for (let i=0; i<10; i++)
		stream.write("item "+i);

	await crawlutil.sleep(100);

	for (let i=0; i<10; i++) {
		try {
			console.log(await stream.read());
		}

		catch(e) {
			console.log("caught...");
		}
	}
/*	stream.read().catch((e)=>{});
	stream.read().catch((e)=>{});
	stream.read().catch((e)=>{});*/

/*	let stream=new astreams.Stream();
	stream.write(new Promise((resolve,reject)=>{
		reject(new Error("hello"));
	}));

	await crawlutil.sleep(100);
	stream.read().catch((e)=>{});*/
}

main();