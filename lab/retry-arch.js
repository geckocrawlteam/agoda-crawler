class AgodaCrawler {
	constructor(crawl) {
		this.crawl=crawl;
		this.inputs=this.crawl.inputs;
		this.numResultsFound=0;
		this.emptyResultFound=false;
		this.startPage=0;
		this.cleanInput();
	}

	cleanInput() {
		this.crawl.input.results=crawlutil.clampNum(this.crawl.input.results,1,2000);
		this.crawl.input.rooms=crawlutil.clampNum(this.crawl.input.rooms,1,10);
		this.crawl.input.daysfromnow=crawlutil.clampNum(this.crawl.input.daysfromnow,1,90);
	}

	async processResult(agodaResult) {
		if (!this.agodaFetcher)
			this.agodaFetcher=new AgodaFetcher(
				this.crawl
			);

		let res;
		try {
			res=await this.agodaFetcher.processResult(agodaResult);
		}
	}

	async getNextListRes() {
		if (!this.agodaLister)
			this.agodaLister=new AgodaLister(
				this.crawl,
				this.inputs.region,
				this.startPage
			);

		let res;
		try {
			res=await this.agodaLister.getListResult();
		}

		catch (e) {
			this.startPage=this.agodaLister.getNumPagesProcessed();
			this.agodaLister=null;
			throw(e);
		}
	}

	isFinished() {
		if (this.emptyResultFound)
			return true;

		if (this.numResultsFound>=this.inputs.results)
			return true;

		return false;
	}

	createEntry(listResult, propertyResult) {
		let res={
			name: listResult.HotelDisplayName,
			price: listResult.DisplayPrice,
			address: propertyResult.hotelInfo.address.full,
		}

		for (let infoGroup of propertyResult.aboutHotel.usefulInfoGroups) {
			//console.log(infoGroup);
			for (let item of infoGroup.items) {
				//console.log(item);
				if (item.fontIcon=="ficon-number-of-rooms")
					res.numberOfRooms=parseInt(item.description);
			}
		}

		let facilities=[];
		for (let featureGroup of propertyResult.aboutHotel.featureGroups) {
			for (let feature of featureGroup.feature) {
				if (feature.symbol=="ficon-yoga-room")
					facilities.push("Yoga");

				if (feature.symbol=="ficon-kitchen" ||
						feature.symbol=="ficon-kitchenette")
					facilities.push("Kitchen");

				if (feature.symbol=="ficon-spa-sauna")
					facilities.push("Spa");

				if (feature.symbol=="ficon-outdoor-pool")
					facilities.push("Pool");
			}
		}

		facilities.sort();
		let facilitiesClean=[];
		for (let facility of facilities)
			if (facilitiesClean.indexOf(facility)<0)
				facilitiesClean.push(facility);

		res.facilities=facilitiesClean.join(", ");

		return res;
	}

	async run() {
		while (!this.isFinished()) {
			let listResult=await crawlutil.callWithRetries(
				this.getNextListRes.bind(this),{
					retries: 4,
					logger: this.crawl.log
				}
			);

			if (listResult) {
				let propertyResult=await crawlutil.callWithRetries(
					this.processResult.bind(this,listResult),{
						retries: 4,
						logger: this.crawl.log
					}
				);

				let entry=this.createEntry(listResult,processResult);
				this.crawl.result(entry);
				this.numResultsFound++;
			}

			else
				this.emptyResultFound=true;
		}
	}
}
