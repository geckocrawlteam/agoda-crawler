function sleep(millis) {
	return new Promise((resolve, reject)=>{
		setTimeout(resolve,millis);
	});
}

function scrollThroughPage(page, step=100, delay=100) {
	return page.evaluate(async (step, delay)=>{
		await new Promise((resolve, reject)=>{
			var i=setInterval(()=>{
				let last=window.pageYOffset;
				window.scrollBy(0,step);
				if (last==window.pageYOffset) {
					clearInterval(i);
					resolve();
				}
			},delay);
		});
	},step,delay);
}

module.exports=async function(crawl) {
	let page=crawl.getPage();
	let result=[];
	let numPages=crawl.input.pages;

	if (!numPages)
		numPages=0;

	if (numPages<1)
		numPages=1;

	if (numPages>5)
		numPages=5;

	await page.goto("https://www.agoda.com");
//	await crawl.injectJquery();

	await page.click(".Searchbox input");
	await page.keyboard.type(crawl.inputs.region);
	await sleep(2000);
	await page.click("h1",{delay: 1000});
	await sleep(2000);

	crawl.progress(20);

	let pageNum=0;
	while (pageNum<numPages) {
		let searchButton=await page.$("button.Searchbox__searchButton");
		let nextButton=await page.$("button#paginationNext");

		if (searchButton && !pageNum) {
			crawl.log("Clicking search button...");
			await Promise.all([
				page.click("button.Searchbox__searchButton"),
				page.waitForNavigation({timeout: 0})
			]);
		}

		else if (nextButton) {
			crawl.log("Clicking next page button...");
			await Promise.all([
				page.click("button#paginationNext"),
				page.waitForNavigation({timeout: 0, waitUntil: "networkidle0"})
			]);
		}

		else {
			crawl.log("Looks like the last page!");
			crawl.done(result);
			return;
		}

		crawl.progress(20+80*((pageNum+.5)/numPages));

		crawl.log("Processing page #"+pageNum);
		await scrollThroughPage(page,1000,1000);

		let numInvisible=1;
		while (numInvisible) {
			numInvisible=await page.evaluate(()=>{
				let els=$(".LazyLoad").not(".is-visible");

				if (els.size())
					els[0].scrollIntoView();

				return els.size();
			});
			//crawl.log("inv: "+numInvisible);
			await sleep(1000);
		}

		var ret=await page.evaluate(()=>{
			var ret=[];

			$("section.hotel-item-box").each(function() {
				ret.push({
					name: $(this).find("h3").text(),
					price: $(this).find(".price-box__price__amount").text(),
				});
			});

			return ret;
		});

		crawl.log("Page #"+pageNum+" results: "+ret.length);

		result=[...result,...ret];
		pageNum++;
		crawl.progress(20+80*(pageNum/numPages));
	}

	crawl.done(result);
};