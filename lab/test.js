function sleep(millis) {
	return new Promise((resolve, reject)=>{
		setTimeout(resolve,millis);
	});
}

async function callWithRetries(f) {
	for (i=0; i<10; i++) {
		try {
			let res=await f();
			return res;
		}

		catch (err) {
			console.log("Retry #"+i+": "+err.toString());
			await sleep(1000*Math.pow(2,i));
		}
	}

	throw new Error("Tried 10 times, giving up");
}

let callnum=0;
function f(p) {
	callnum++;

	if (callnum<5)
		throw new Error("not yet");

	return p;
}

(async ()=>{
	let res=await callWithRetries(f.bind(null,"123"));
	console.log("res: "+res);
})();
