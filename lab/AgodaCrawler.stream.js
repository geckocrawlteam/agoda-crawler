const crawlutil=require("./crawlutil.js");
const XhrCap=require("./XhrCap.js");
const moment=require("moment");
const AsyncStream=require("./AsyncStream.js");

class AgodaCrawl {
	constructor(crawl, skipPages) {
		this.crawl=crawl;
		this.page=crawl.page;
		this.resultPageOpen=false;
		this.stream=new FilterStream();
		this.stream.setFilter(this.processResult.bind(this));

		this.onResultListResponse=this.onResultListResponse.bind(this);
		this.onResultListFailed=this.onResultListFailed.bind(this);
	}

	async ensureJqueryInjected() {
		if (this.injectJqueryPromise)
			return this.injectJqueryPromise;

		this.injectJqueryPromise=new Promise((resolve, reject)=>{
			this.crawl.injectJquery()
				.then(resolve)
				.catch(reject);
		});
	}

	async processResult(agodaResult) {
		await this.ensureJqueryInjected();

		let url=
			"/api/en-us/pageparams/property?hotel_id="
			+agodaResult.HotelID;

		let propertyResult=await crawlutil.ajaxRequestInPage(this.page,{
			url: url,
		});

		return {
			agodaResult: agodaResult,
			propertyResult: propertyResult
		};
	}

	async getResultUrlForSearch(search) {
		await this.page.goto("http://www.agoda.com/",{waitUntil: "domcontentloaded"});
		this.crawl.log("Initial page loaded");

		// This is on the main front page, no risk of injecting twice.
		await this.crawl.injectJquery();

		let res=await crawlutil.ajaxRequestInPage(this.page,{
			url: "/Search/Search/GetUnifiedSuggestResult/3/1/1/0/en-us/",
			data: {
				searchText: search
			}
		});

		await this.page.evaluate(()=>{
			window.stop();
		});

		if (res.SuggestionList.length<1)
			throw new crawlutil.NoRetryError("No search results found for "+search);

		let suggestionItem=res.SuggestionList[0];
		if (!suggestionItem.Url)
			throw new crawlutil.NoRetryError("No search results found for "+search);

		//this.crawl.progress(10);

		let checkIn=moment().add(this.crawl.input.daysfromnow,"days");

		return "https://www.agoda.com"+suggestionItem.Url+
			"&rooms="+this.crawl.input.rooms+
			"&adults="+this.crawl.input.rooms+
			"&checkIn="+checkIn.format("YYYY-MM-DD")+
			"&checkOut="+checkIn.add(1,"days").format("YYYY-MM-DD");
	}

	onResultListResponse(response, body) {
		let res=JSON.parse(body);
		this.crawl.log("Search response page #"+res.PageNumber+": "+res.ResultList.length);

		for (let listResult of res.ResultList)
			this.stream.write(listResult);

		if (res.ResultList.length==0)
			this.stream.write(null);
	}

	onResultListFailed(err) {
		this.stream.write(err);
	}

	async isOnFinalPage() {
		function isFinalPageRendered() {
			function extractNumberTokens(s) {
				let a=s.split(" ");
				let res=[];

				for (candString of a) {
					let cand=parseInt(candString)
					if (!isNaN(cand) && cand!=0)
						res.push(cand);
				}

				return res;
			}

			let parts=extractNumberTokens($("#paginationContainer").text());
			if (parts.length==2 && parts[0]==parts[1])
				return true;

			return false;
		}

		return await this.page.evaluate(isFinalPageRendered);
	}

	async waitForNavigationElements() {
		function isPageNavigationRendered() {
			function extractNumberTokens(s) {
				let a=s.split(" ");
				let res=[];

				for (candString of a) {
					let cand=parseInt(candString)
					if (!isNaN(cand) && cand!=0)
						res.push(cand);
				}

				return res;
			}

			if ($("#paginationNext").length)
				return true;

			let parts=extractNumberTokens($("#paginationContainer").text());
			if (parts.length==2 && parts[0]==parts[1])
				return true;

			return false;
		}

		await this.page.waitForFunction(isPageNavigationRendered);
	}

	async run() {
		let url=await this.getResultUrlForSearch(this.crawl.input.region);

		this.xhrCap=new XhrCap(this.page);
		this.xhrCap.setConsoleLogger(this.crawl.log);
		this.xhrCap.setFilter("GetSearchResultList");
		this.xhrCap.on("response",this.onResultListResponse);
		this.xhrCap.on("requestfailed",this.onResultListFailed);

		let p=await Promise.all([
			this.page.goto(url,{waitUntil: "domcontentloaded"}),
			this.xhrCap.waitForResponse()
		]);

		await this.ensureJqueryInjected();

		while (1) {
			await this.waitForNavigationElements();
			let isOnFinalPage=await this.isOnFinalPage();
			if (isOnFinalPage
					|| this.stream.isEof()
					|| this.stream.isError()
					|| this.stream.getNumWritten()>=this.crawl.input.results) {
				this.crawl.log("Writing EOF to stream");
				this.crawl.log("- isOnFinalPage: "+isOnFinalPage);
				this.crawl.log("- eof: "+this.listStream.isEof());
				this.crawl.log("- error: "+this.listStream.isError());
				this.crawl.log("- num: "+this.listStream.getNumWritten());
				this.crawl.log("- res: "+this.listStream.input.results);
				this.stream.write(null);
				return;
			}

			await this.xhrCap.waitForIdle();
			let p=await Promise.all([
				this.page.evaluate(()=>{
					$("#paginationNext").click();
				}),
				this.xhrCap.waitForResponse()
			]);
		}
	}

	start() {
		this.run()
			.then(()=>{})
			.catch((e)=>{
				this.stream.write(e);
			});
	}

	async read() {
		return await this.stream.read();
	}
}

module.exports=AgodaCrawl;