rp=require("request-promise");
uuid=require("uuid/v4");

let crawl;

class AgodaCrawl {
	constructor() {
		this.currentDate=new Date().toISOString();
		let time=new Date().getTime();

		this.checkIn=new Date(time+1000*60*60*24*7).toISOString();
		this.checkOut=new Date(time+1000*60*60*24*8).toISOString();
		this.searchMessageId=uuid();
	}

	async getPageResults(pageNum) {
		let reqParams=require("./reqparams.json");

		reqParams.SearchMessageID=this.searchMessageId;
		reqParams.CheckIn=this.checkIn;
		reqParams.CheckOut=this.checkOut;

		reqParams.CityId=this.cityId;

		reqParams.CityName=this.regionName;
		reqParams.ObjectName=this.regionName;
		reqParams.CityEnglishName=this.regionName;
		reqParams.Text=this.regionName;

		let res=await rp({
			url: "https://www.agoda.com/api/en-us/Main/GetSearchResultList",
			method: "POST",
			json: reqParams
		});

		console.log(res);

		let resultList=res.ResultList;
		for (let result of resultList) {
			crawl.log(result.HotelDisplayName);
		}
	}

	async initRegion(region) {
		let res=await rp({
			uri: "https://www.agoda.com/Search/Search/GetUnifiedSuggestResult/3/1/1/0/en-us/",
			json: true,
			qs: {
				searchText: region
			}
		});

		let suggestionItem=res.SuggestionList[0];
		this.cityId=suggestionItem.ObjectID;
		this.regionName=suggestionItem.Name;

		console.log(suggestionItem);

		if (!this.cityId || !this.regionName)
			throw new Error("Not found");

		process.exit(0);

		/*console.log(suggestionItem);

		return suggestionItem.ObjectID;*/
	}
}

// https://www.agoda.com/api/en-us/pageparams/property?hotel_id=1165111
// usefulInfoGroups

async function getResults() {
	let reqParams=require("./reqparams.json");
	let res=await rp({
		url: "https://www.agoda.com/api/en-us/Main/GetSearchResultList",
		method: "POST",
		json: reqParams
	});

	let resultList=res.ResultList;
	crawl.log("results: "+resultList.length);

	for (result of resultList) {
		crawl.log(result.HotelDisplayName);
	}

//	console.log(resultList[0]);
}

module.exports=async function(c) {
	crawl=c;
	let result=[];

	let agodaCrawl=new AgodaCrawl();

//	console.log(agodaCrawl);
	await agodaCrawl.initRegion(crawl.input.region);
	crawl.progress(10);

	await agodaCrawl.getPageResults(1);



/*	await getResults();*/

/*	let cityId=await getCityId(crawl.input.region);
	crawl.log("CityId: "+cityId);*/

	crawl.done(result);
};