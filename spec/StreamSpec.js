const astreams=require("../astreams/astreams.js");
const crawlutil=require("../crawlutil.js");

describe("AsyncStream",()=>{
	it("can read and write",async ()=>{
		let stream=new astreams.Stream();

		stream.write("hello");
		expect(await stream.read()).toEqual("hello");
	});
	it("can wait",(done)=>{
		let stream=new astreams.Stream();
		stream.read().then((o)=>{
			expect(o).toEqual("hello");
			done();
		});
		stream.write("hello");
	});
	it("test",async ()=>{
		let stream=new astreams.Stream();
		setTimeout(()=>{
			for (let i=0; i<20; i++)
				stream.write("hello "+i);
		},500);

		for (i=0; i<20; i++)
			expect(await stream.read()).toEqual("hello "+i);
	});
	it("can handle promises",async ()=>{
		let stream=new astreams.Stream();
		stream.write(new Promise((resolve,reject)=>{
			resolve("hello");
		}));
		let x=await stream.read();
		expect(x).toEqual("hello");
	});
	it("can handle rejected promises",async ()=>{
		let stream=new astreams.Stream();
		stream.write(new Promise((resolve,reject)=>{
			reject(new Error("hello"));
		}));

		await crawlutil.sleep(100);
		stream.read().catch((e)=>{});
	});
});