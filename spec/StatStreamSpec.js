const astreams=require("../astreams/astreams.js");

function tick() {
	return new Promise((resolve)=>{
		process.nextTick(resolve);
	});
}

describe("StatStream",()=>{
	it("can count",async ()=>{
		let stream=new astreams.StatStream(new astreams.Stream());

		stream.write("hello");
		stream.write("world");
		expect(await stream.read()).toEqual("hello");
		expect(stream.getNumWritten()).toEqual(2);
		expect(stream.isWriteOpen()).toEqual(true);

		stream.write(new Error());
		expect(stream.getNumWritten()).toEqual(2);
		expect(stream.isWriteOpen()).toEqual(false);
	});
	it("measures watermark",async ()=>{
		let stream=new astreams.StatStream(new astreams.Stream());

		stream.write("hello");
		stream.write(new Error("world"));

		expect(stream.getNumWritten()).toEqual(1);
		expect(stream.getWaterMark()).toEqual(2);

		await stream.read();
		expect(stream.getWaterMark()).toEqual(1);
		await expectAsync(stream.read()).not.toBeResolved();
		expect(stream.getWaterMark()).toEqual(0);
	});

	it("can drain",async ()=>{
		let o={func: ()=>{}};
		spyOn(o,"func");

		let stream=new astreams.StatStream(new astreams.Stream());

		stream.write("hello");
		stream.write("world");

		stream.drain(2).then(o.func);
		stream.drain(1).then(o.func);
		stream.drain(0).then(o.func);
		expect(stream.listenerCount("waterMarkChange")).toEqual(2);

		await tick();
		expect(o.func.calls.count()).toEqual(1);
		expect(stream.listenerCount("waterMarkChange")).toEqual(2);

		await stream.read();
		expect(o.func.calls.count()).toEqual(2);
		expect(stream.listenerCount("waterMarkChange")).toEqual(1);

		await stream.read();
		expect(o.func.calls.count()).toEqual(3);
		expect(stream.listenerCount("waterMarkChange")).toEqual(0);
	});
});