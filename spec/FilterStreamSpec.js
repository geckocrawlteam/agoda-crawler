const astreams=require("../astreams/astreams.js");
const crawlutil=require("../crawlutil.js");

describe("FilterStream",()=>{
	it("can write through a filter",async ()=>{
		let stream=new astreams.FilterStream();
		stream.setNumParallell(1);
		let filterCalled=0;

		stream.setFilter((o)=>{
			return new Promise((resolve,reject)=>{
				filterCalled++;
				setTimeout(()=>{
					resolve(o+1);
				},100);
			});
		});

		stream.write(1);
		stream.write(2);
		stream.write(3);
		stream.write(new Error("test"));
		stream.write(new Promise((resolve, reject)=>{
			setTimeout(resolve.bind(null,123),200);
		}));
		stream.write(null);

		await crawlutil.sleep(100);

		expect(await stream.read()).toEqual(2);
		expect(await stream.read()).toEqual(3);
		expect(await stream.read()).toEqual(4);

		let called=false;
		await stream.read()
			.catch((e)=>{
				called=true;
			});

		expect(called).toEqual(true);
		expect(await stream.read()).toEqual(124);
		expect(await stream.read()).toEqual(null);
		expect(filterCalled).toEqual(4);
	});

	it("pushes error from the filter onto the stream",async ()=>{
		let stream=new astreams.FilterStream(async (x)=>{
			throw new Error("bla");
		});

		stream.write("item 1");
		stream.write("item 2");
		stream.write("item 3");

		await crawlutil.sleep(100);

		await stream.read().catch(()=>{});
		await stream.read().catch(()=>{});
		await stream.read().catch(()=>{});
	});
})