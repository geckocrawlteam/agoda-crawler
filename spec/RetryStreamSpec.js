const astreams=require("../astreams/astreams.js");

describe("RetryStream",()=>{
	it("can produce a stream and read",async ()=>{
		let stream;

		function factory() {
			stream=new astreams.Stream();
			stream.write("test");
			return stream;
		}

		setTimeout(()=>{
			stream.write("hello");
			stream.write("world");
		},100);

		let retryStream=new astreams.RetryStream();
		retryStream.setFactory(factory);

		expect(await retryStream.read()).toEqual("test");
		expect(await retryStream.read()).toEqual("hello");
		expect(await retryStream.read()).toEqual("world");
	});

	it("calls the factory on error",async ()=>{
		let factoryCalls=0;
		async function factory() {
			let stream=new astreams.Stream();

			switch (factoryCalls) {
				case 0:
					stream.write("top");

					setTimeout(()=>{
						stream.write("hello");
						stream.write("world");
						stream.write(new Error("some error"));
						stream.write("should not be seen");
					},100);
					break;

				case 1:
					stream.write("recovered");
					setTimeout(()=>{
						stream.write("hello again");
					},100);
					break;
			}

			factoryCalls++;
			return stream;
		}

		let retryStream=new astreams.RetryStream();
		retryStream.setFactory(factory);

		let a=[];
		retryStream.setLogger((message)=>{
			a.push(message);
		});

		expect(await retryStream.read()).toEqual("top");
		expect(await retryStream.read()).toEqual("hello");
		expect(await retryStream.read()).toEqual("world");
		expect(await retryStream.read()).toEqual("recovered");
		expect(await retryStream.read()).toEqual("hello again");

		//console.log(a);
		expect(a.length).toBeGreaterThan(0);
		expect(factoryCalls).toEqual(2);
	});

	it("resets the retry count on produced items",async ()=>{
		let callNum=0;
		function factory() {
			//console.log("Factory #"+callNum);
			let stream=new astreams.Stream();

			switch (callNum) {
				case 2:
					stream.write("hello");
					break;

				case 4:
					stream.write("world");
					stream.write(null);
					break;
			}

			callNum++;
			return stream;
		}
		let retryStream=new astreams.RetryStream(factory);
		retryStream.setRetries(3);
		retryStream.setTimeout(100);
		//retryStream.setLogger(console.log);
		expect(await retryStream.read()).toEqual("hello");
		expect(await retryStream.read()).toEqual("world");
		expect(await retryStream.read()).toEqual(null);
	});

	it("doesn't retry on fatal errors",async ()=>{
		class FatalError extends Error {};

		let o={
			factory: ()=>{
				let stream=new astreams.Stream();
				stream.write(new FatalError("test"));
				return stream;
			}
		}

		spyOn(o,"factory").and.callThrough();

		let retryStream=new astreams.RetryStream(o.factory);
		retryStream.addFatal(FatalError);
		//retryStream.setLogger(console.log);

		let caught;
		try {
			await retryStream.read();
		}

		catch (e) {
			caught=true;
		}

		expect(caught).toEqual(true);
		expect(o.factory.calls.count()).toEqual(1);
	});
});