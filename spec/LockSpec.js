const Lock=require("../astreams/Lock.js");
const crawlutil=require("../crawlutil.js");

describe("Lock",()=>{
	it("can be locked",async ()=>{
		let o={test: ()=>{}};
		spyOn(o,"test");

		let l=new Lock();
		l.setNumParallell(1);

		l.aquire().then(o.test);
		await crawlutil.sleep(100);
		expect(o.test.calls.count()).toEqual(1);

		l.aquire().then(o.test);
		await crawlutil.sleep(100);
		expect(o.test.calls.count()).toEqual(1);

		l.release();
		await crawlutil.sleep(100);
		expect(o.test.calls.count()).toEqual(2);

		l.release();
		await crawlutil.sleep(100);

		expect(()=>{
			l.release();
		}).toThrow();
	});
});