const AgodaCrawlStream=require("../AgodaCrawlStream.js");

describe("AgodaCrawlStream",()=>{
	it("can convert results",()=>{
		let mockCrawl={
			page: null
		};

		let agodaCrawlStream=new AgodaCrawlStream(mockCrawl);
		let listResult={};
		let propertyResult=require("./data/sample-property-response.json");
		let entry=agodaCrawlStream.createEntry(listResult,propertyResult);
		expect(entry.facilities).toEqual("Pool, Spa, Yoga");
		//console.log(entry);
	});
})