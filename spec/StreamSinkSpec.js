const astreams=require("../astreams/astreams.js");

describe("StreamSink",()=>{
	it("converts streams to events",async ()=>{
		let stream=new astreams.Stream();
		let out=new astreams.Stream();
		stream.write("hello");
		let sink=new astreams.StreamSink(stream);
		sink.on("item",(o)=>{
			out.write(o);
		});
		stream.write("world");

		expect(await out.read()).toEqual("hello");
		expect(await out.read()).toEqual("world");
	});
});