const NetSpy=require("../NetSpy.js");

class MockRequest {
	constructor(url) {
		this._url=url;
	}

	url() {
		return this._url;
	}
}

describe("NetSpy",()=>{
	it("can sluggify a url",()=>{
		let spy=new NetSpy();
		let r;

		r=new MockRequest("http://x.com/y");
		expect(spy.getRequestSlug(r)).toEqual("y");

		r=new MockRequest("http://x.com/a/b/c/");
		expect(spy.getRequestSlug(r)).toEqual("c");

		r=new MockRequest("http://x.com/");
		expect(spy.getRequestSlug(r)).toEqual("");
	});
});