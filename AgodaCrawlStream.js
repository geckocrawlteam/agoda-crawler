const crawlutil=require("./crawlutil.js");
const XhrCap=require("./XhrCap.js");
const moment=require("moment");
const astreams=require("./astreams/astreams.js");
const EventEmitter=require("events");

class AgodaCrawlStream extends EventEmitter {
	constructor(crawl, skipResults) {
		super();

		this.crawl=crawl;
		this.page=crawl.page;
		this.skipResults=skipResults;
		this.skippedResults=0;
		this.resultListFails=0;

		let filterStream=new astreams.FilterStream();
		filterStream.setNumParallell(3);
		filterStream.setFilter(this.processResult.bind(this));
		this.stream=new astreams.StatStream(filterStream);

		this.onResultListResponse=this.onResultListResponse.bind(this);
		this.onResultListFailed=this.onResultListFailed.bind(this);
	}

	async ensureJqueryInjected() {
		if (this.injectJqueryPromise)
			return this.injectJqueryPromise;

		this.injectJqueryPromise=new Promise((resolve, reject)=>{
			this.crawl.injectJquery()
				.then(resolve)
				.catch(reject);
		});

		return this.injectJqueryPromise;
	}

	createEntry(listResult, propertyResult) {
		let res={
			name: listResult.HotelDisplayName,
			price: listResult.DisplayPrice,
			address: propertyResult.hotelInfo.address.full,
		}

		for (let infoGroup of propertyResult.aboutHotel.usefulInfoGroups) {
			//console.log(infoGroup);
			for (let item of infoGroup.items) {
				//console.log(item);
				if (item.fontIcon=="ficon-number-of-rooms")
					res.numberOfRooms=parseInt(item.description);
			}
		}

		let facilities=[];
		for (let featureGroup of propertyResult.aboutHotel.featureGroups) {
			for (let feature of featureGroup.feature) {
				let sym;

				if (feature.formattedIconCss)
					sym=feature.formattedIconCss;

				if (feature.symbol)
					sym=feature.symbol;

				if (feature.iconCss)
					sym=feature.iconCss;

				if (sym=="ficon-yoga-room")
					facilities.push("Yoga");

				if (sym=="ficon-kitchen" ||
						sym=="ficon-kitchenette")
					facilities.push("Kitchen");

				if (sym=="ficon-spa-sauna")
					facilities.push("Spa");

				if (sym=="ficon-outdoor-pool")
					facilities.push("Pool");
			}
		}

		facilities.sort();
		let facilitiesClean=[];
		for (let facility of facilities)
			if (facilitiesClean.indexOf(facility)<0)
				facilitiesClean.push(facility);

		res.facilities=facilitiesClean.join(", ");
		return res;
	}

	async processResult(listResult) {
		/*if (!this.delivered)
			this.delivered=0;

		this.delivered++;

		if (this.delivered>=300) {
			throw new Error("random error");
		}*/

		await this.ensureJqueryInjected();

		//this.crawl.log("Fetching details: "+listResult.HotelID);

		let url=
			"/api/en-us/pageparams/property?hotel_id="
			+listResult.HotelID;

		let propertyResult=await crawlutil.ajaxRequestInPage(this.page,{
			url: url,
		});

		//this.crawl.log("Got result for: "+listResult.HotelID);

		return this.createEntry(listResult,propertyResult);
	}

	async getResultUrlForSearch(search) {
		await this.page.goto("http://www.agoda.com/",{waitUntil: "domcontentloaded"});
		this.crawl.log("Initial page loaded");

		// This is on the main front page, no risk of injecting twice.
		await this.crawl.injectJquery();

		let res=await crawlutil.ajaxRequestInPage(this.page,{
			url: "/Search/Search/GetUnifiedSuggestResult/3/1/1/0/en-us/",
			data: {
				searchText: search
			}
		});

		/*await this.page.evaluate(()=>{
			window.stop();
		});*/

		if (res.SuggestionList.length<1)
			throw new crawlutil.NoRetryError("No search results found for "+search);

		let suggestionItem=res.SuggestionList[0];
		if (!suggestionItem.Url)
			throw new crawlutil.NoRetryError("No search results found for "+search);

		let checkIn=moment().add(this.crawl.input.daysfromnow,"days");

		return "https://www.agoda.com"+suggestionItem.Url+
			"&rooms="+this.crawl.input.rooms+
			"&adults="+this.crawl.input.rooms+
			"&checkIn="+checkIn.format("YYYY-MM-DD")+
			"&checkOut="+checkIn.add(1,"days").format("YYYY-MM-DD");
	}

	onResultListResponse(response, body) {
		this.emit("activity");

		let res=JSON.parse(body);

		for (let listResult of res.ResultList) {
			if (this.skippedResults<this.skipResults) {
				this.skippedResults++;
			}

			else {
				/*if (this.stream.getNumWritten()>5) {
					this.crawl.log("emulating error...");
					this.stream.write(new Error("random error"));
				}*/

				if (this.stream.isWriteOpen()) {
					if (this.stream.getNumWritten()+this.skippedResults>=
							this.crawl.input.results)
						this.stream.write(null);

					else
						//if (this.stream.getNumWritten()<5)
							this.stream.write(listResult);
				}
			}
		}

		this.crawl.log("Search response page #"+res.PageNumber+
			": "+res.ResultList.length+" results, "+
			"written: "+this.stream.getNumWritten()+", "+
			"skipped: "+this.skippedResults);
	}

	onResultListFailed(err) {
		this.resultListFails++;

		if (this.resultListFails>4)
			this.stream.write(err);
	}

	async isOnFinalPage() {
		function isFinalPageRendered() {
			function extractNumberTokens(s) {
				let a=s.split(" ");
				let res=[];

				for (candString of a) {
					let cand=parseInt(candString)
					if (!isNaN(cand) && cand!=0)
						res.push(cand);
				}

				return res;
			}

			let parts=extractNumberTokens($("#paginationContainer").text());
			if (parts.length==2 && parts[0]==parts[1])
				return true;

			return false;
		}

		return await this.page.evaluate(isFinalPageRendered);
	}

	async waitForNavigationElements() {
		function isPageNavigationRendered() {
			function extractNumberTokens(s) {
				let a=s.split(" ");
				let res=[];

				for (candString of a) {
					let cand=parseInt(candString)
					if (!isNaN(cand) && cand!=0)
						res.push(cand);
				}

				return res;
			}

			if ($("#paginationNext").length)
				return true;

			let parts=extractNumberTokens($("#paginationContainer").text());
			if (parts.length==2 && parts[0]==parts[1])
				return true;

			return false;
		}

		await this.page.waitForFunction(isPageNavigationRendered);
	}

	async run() {
		this.crawl.log("**** Starting crawl, skipping results: "+this.skipResults);

		let url=await this.getResultUrlForSearch(this.crawl.input.region);

		this.xhrCap=new XhrCap(this.page);
		this.xhrCap.setConsoleLogger(this.crawl.log);
		this.xhrCap.setFilter("GetSearchResultList");
		this.xhrCap.on("response",this.onResultListResponse);
		this.xhrCap.on("requestfailed",this.onResultListFailed);

		this.page.setDefaultTimeout(120000);

		let p=await Promise.all([
			this.page.goto(url,{waitUntil: "domcontentloaded"}),
			this.xhrCap.waitForResponse()
		]);

		await this.ensureJqueryInjected();

		while (1) {
			await this.waitForNavigationElements();
			if (await this.isOnFinalPage()) {
				this.crawl.log("Final page detected.");
				this.stream.write(null);
			}

			if (!this.stream.isWriteOpen())
				return;

			await this.stream.drain(100);
			await this.xhrCap.waitForIdle();
			let p=await Promise.all([
				this.page.evaluate(()=>{
					$("#paginationNext").click();
				}),
				this.xhrCap.waitForResponse()
			]);
		}
	}

	start() {
		this.run()
			.then(()=>{})
			.catch((e)=>{
				this.stream.write(e);
			});
	}

	async stop() {
		this.stream.write(null);
		if (this.xhrCap)
			this.xhrCap.detach();

/*		return await this.page.evaluate(()=>{
			window.stop();
		});*/
	}

	async read() {
		return await this.stream.read();
	}
}

module.exports=AgodaCrawlStream;