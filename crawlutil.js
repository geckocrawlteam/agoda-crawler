class NoRetryError extends Error {};

function sleep(millis) {
	return new Promise((resolve, reject)=>{
		setTimeout(resolve,millis);
	});
}

async function callWithRetries(f, opts) {
	if (!opts)
		opts={};

	if (!opts.logger)
		opts.logger=console.log;

	if (!opts.retries)
		opts.retries=10;

	for (i=0; i<opts.retries; i++) {
		try {
			let res=await f();
			return res;
		}

		catch (err) {
			if (err instanceof NoRetryError) {
				opts.logger("Will not retry...");
				throw err;
			}

			opts.logger("Retry #"+i+": "+err.toString());
			await sleep(1000*Math.pow(2,i));
		}
	}

	throw new Error("Tried "+opts.retries+" times, giving up");
}

/*async function scrollThroughPage(page, step=100, delay=100) {
	return page.evaluate(async (step, delay)=>{
		await new Promise((resolve, reject)=>{
			var i=setInterval(()=>{
				let last=window.pageYOffset;
				window.scrollBy(0,step);
				if (last==window.pageYOffset) {
					clearInterval(i);
					resolve();
				}
			},delay);
		});
	},step,delay);
}*/

async function scrollToBottom(page) {
	return page.evaluate(async ()=>{
		window.scrollTo(0,$(document).height());
	});
}

async function ajaxRequestInPage(page, ajaxParams) {
	return await page.evaluate((ajaxParams)=>{
		return new Promise((resolve, reject)=>{
			ajaxParams.success=resolve;
			ajaxParams.error=(err)=>{reject(JSON.stringify(err))};
			$.ajax(ajaxParams);
		});
	},ajaxParams);
}

function clampNum(num, min, max) {
	num=parseInt(num);
	if (!num)
		num=0;

	if (num<min)
		num=min;

	if (num>max)
		num=max;

	return num;
}

module.exports={
	NoRetryError: NoRetryError,
	sleep: sleep,
	callWithRetries: callWithRetries,
	scrollToBottom: scrollToBottom,
	ajaxRequestInPage: ajaxRequestInPage,
	clampNum: clampNum
};
